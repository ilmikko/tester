. "$DIR/lib/assert.sh" || exit 222;
. "$DIR/lib/logging.sh" || exit 223;
. "$DIR/lib/runtime.sh" || exit 224;
. "$DIR/lib/output.sh" || exit 229;
. "$DIR/lib/skip.sh" || exit 225;

. "$DIR/lib/test.sh" || exit 228;
. "$DIR/lib/unit.sh" || exit 226;
. "$DIR/lib/integration.sh" || exit 227;

filters="";

while [ $# -gt 0 ]; do
	case $1 in
		--integration) # Only run integration tests.
			# Unit tests are skipped.
			run_unit_tests=0;
			;;
		--unit) # Only run unit tests.
			# Integration tests are skipped.
			run_integration_tests=0;
			;;
		--output) # Set the output type.
			# This can be 'raw', 'short' or 'detailed'.
			shift;
			output_set_type "$1";
			;;
		--)
			shift;
			break;
			;;
		--*)
			fail "Unknown command: $1";
			;;
		*)
			filters="$filters $1";
			;;
	esac
	shift;
done

# You can filter tests using this simple trick! Doctors hate them!
filters=$(echo $filters | tr ' ' '|');

if [ -z "$OUTPUT_TYPE" ]; then
	if [ -z "$filters" ]; then
		output_set_type short;
	else
		output_set_type detailed;
	fi
fi
