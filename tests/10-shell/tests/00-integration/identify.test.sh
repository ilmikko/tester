something_WRONG_not_test_but_looks_like_it() {
	echo NO;
	exit 1;
}

function something_WRONG_not_test_but_looks_like_it() {
	echo NO;
	exit 1;
}

function something_WRONG_not_test_but_looks_like_it {
	echo NO;
	exit 1;
}

function test_RIGHT1_this {
	echo YES;
	exit 1;
}

function test_RIGHT2_me_too  () {
	echo YES;
	exit 1;
}

function test_RIGHT3_this_too() {
	echo YES;
	exit 1;
}

function not_WRONG_for_testing() {
	echo NO;
	exit 1;
}

this_is_RIGHT4_a_test() {
	echo YES;
	exit 1;
}

test_RIGHT5_this_too()   { # Some comment
	echo YES;
	exit 1;
}

test_RIGHT6_me  () {
	echo YES;
	exit 1;
}

function not_testing_WRONG_for_real    () { # this is not_a_test
	echo NO;
	exit 1;
}
