# Test that shell test functions are identified correctly.

assert_exec_find "./tester.sh identify" "RIGHT1" "RIGHT2" "RIGHT3" "RIGHT4" "RIGHT5" "RIGHT6";
assert_not assert_exec_find "./tester.sh identify" "WRONG";
