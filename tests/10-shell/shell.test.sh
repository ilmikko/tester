# Check that shell tests work as they are supposed to.
# Running a shell test is simply executing the .test.sh file.
assert_exec "./tester.sh pass" 0;
assert_exec "./tester.sh fail" 1;
assert_exec "./tester.sh assert" 1;
assert_exec "./tester.sh multiple" 1;

assert_exec_find "./tester.sh multiple" "Broken!" "FAIL" "test_something_working" "test_something_borke";
