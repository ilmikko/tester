# We should always execute a .test file that resides in our project folder.
# Only the exit code matters.
assert_exec "./tester.sh pass" 0;
assert_exec "./tester.sh fail" 1;
