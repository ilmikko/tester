test_raw_output() {
	raw_output=$(./tester.sh --output raw 2>&1);
	assert_find "$raw_output" "EXEC ./tests/00-simple/pass.test.sh" "PASS ./tests/00-simple/pass.test.sh";
	assert_find "$raw_output" "EXEC ./tests/00-simple/fail.test.sh" "FAIL ./tests/00-simple/fail.test.sh";
	assert_find "$raw_output" "EXEC ./tests/10-subtest/pass.test.sh" "PASS ./tests/10-subtest/pass.test.sh"
	assert_find "$raw_output" "EXEC ./tests/10-subtest/fail.test.sh" "FAIL ./tests/10-subtest/fail.test.sh"
	# Raw output should have passing test output as well.
	assert_find "$raw_output" "Output from passing test." "Output from failing test."
	assert_find "$raw_output" "Output from a passing subtest." "Output from a failing subtest."
	assert_find "$raw_output" "Output from a passing subtest in a failing test." "Output from passing subtest in a passing test."
}

test_detailed_output() {
	# Detailed output.
	detailed_output=$(./tester.sh --output detailed 2>&1);
	# If there are no subtests, we simply say whether a test passes or fails.
	assert_find "$detailed_output" "^./tests/00-simple/pass.test.sh \[.\{1,4\}mPASS";
	assert_find "$detailed_output" "^./tests/00-simple/fail.test.sh \[.\{1,4\}mFAIL";

	# Raw output sdetailedld have passing test output as well.
	assert_find "$detailed_output" "Output from failing test."
	assert_not assert_find "$detailed_output" "Output from passing test."

	# In subtests, we should expand the lines further.
	assert_find "$detailed_output" "^./tests/10-subtest/pass.test.sh ::" "test_pass_pass \[.\{1,4\}mPASS" "test_pass_other \[.\{1,4\}mPASS"
	assert_find "$detailed_output" "^./tests/10-subtest/fail.test.sh ::" "test_fail_pass \[.\{1,4\}mPASS" "test_fail_fail \[.\{1,4\}mFAIL";

	assert_not assert_find "$detailed_output" "Output from a passing subtest."
	assert_not assert_find "$detailed_output" "Output from passing subtest in a passing test."
	assert_not assert_find "$detailed_output" "Output from a passing subtest in a failing test."
	assert_find "$detailed_output" "Output from a failing subtest."
}
