echo "Output before subtests.";

test_pass_pass() {
	echo "Output from passing subtest in a passing test.";
}

echo "Output outside of subtests.";

test_pass_other() {
	echo "Output from other passing subtest in a passing test.";
}

echo "Output after subtests.";
