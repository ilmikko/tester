echo "Output before failing subtests.";

test_fail_pass() {
	echo "Output from a passing subtest in a failing test.";
}

echo "Output outside of failing subtests.";

test_fail_fail() {
	echo "Output from a failing subtest.";
	exit 1;
}

echo "Output after failing subtests.";
