# This plays out how someone who is using tester the first time would get started.
assert_exec "./tester.sh" 0;
assert_exec_find "./tester.sh" "No tests were run";

mkdir -p "./tests/00-integration-tests";
assert_exec "./tester.sh" 0;
assert_exec_find "./tester.sh" "No tests were run";

echo exit 0 > "./tests/00-integration-tests/pass.test.sh";
assert_exec "./tester.sh" 0;
assert_exec_find "./tester.sh" "All tests pass";

rm -rf "./tests";
