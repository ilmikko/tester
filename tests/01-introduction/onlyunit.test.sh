# Maybe someone wants only unit tests. This scenario is for them.

# TODO: This is a bit counterintuitive, maybe say "no tests found" instead?
assert_exec "./tester.sh" 0;
assert_exec_find "./tester.sh" "No tests were run";

cat > "someunit.test.sh" <<HERE
echo I have failed
exit 1
HERE

assert_exec "./tester.sh" 1;
assert_exec_find "./tester.sh" "Failures during testing" "someunit.test.sh" "Failure log" "I have failed";

rm "someunit.test.sh";
