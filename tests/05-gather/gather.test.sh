# Test that integration and unit tests are gathered correctly.
# All tests that are inside */tests/** are considered integration tests.
# Every other test is considered a unit test.
# 
# That is:
# ./tests/integration-01/some.test.sh is an integration test.
# ./tests/some.test.sh is a unit test.
# ./some.test.sh is a unit test.

# TODO: Make this test less reliant on the output, as it only should test which test is integration and which is unit.
# TODO 2: Actually this would much better be a unit test.

! ! assert_equals "$(./tester --output raw 2>&1)" "$(cat <<HERE
[1mRunning unit tests:[m
EXEC ./tests/unit.test.sh
PASS ./tests/unit.test.sh
EXEC ./some/other/component/tests/unit.test.sh
PASS ./some/other/component/tests/unit.test.sh
EXEC ./some/other/component/unit.test.sh
PASS ./some/other/component/unit.test.sh
[1m[m
[1mRunning integration tests:[m
EXEC ./tests/integration/integration.test.sh
PASS ./tests/integration/integration.test.sh
EXEC ./tests/integration-02/integration.test.sh
PASS ./tests/integration-02/integration.test.sh
[1m[m
SUMM 5 5 0 0
PASS
HERE
)" || exit 1;
