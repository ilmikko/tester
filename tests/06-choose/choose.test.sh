# Test that we can run unit tests or integration tests with command line flags.

assert_exec "./tester.sh --unit" 0;
assert_exec "./tester.sh --integration" 1;

assert_exec_find "./tester --unit ." "some.unit.test.sh";
assert_exec_find "./tester . --integration" "some.integration.test.sh" "I am an integration test.";
