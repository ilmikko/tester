# Check that Go tests work as they are supposed to.
assert_exec "./tester.sh pass" 0;
assert_exec "./tester.sh fail" 1;
