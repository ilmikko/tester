/**
 * pass.test.c
 * This test will pass!
 * */

package main

import (
	"fmt"
)

func main() {
	fmt.Printf("This test is a pass!\n")
}
