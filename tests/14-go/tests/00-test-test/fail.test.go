/**
 * fail.test.go
 * This test will fail!
 * */

package main

import (
	"log"
)

func main() {
	log.Fatalf("Test failure!")
}
