def passing_method_test
  p "This method is fine.";
end

def failing_method_test
  p "This method is not.";
  raise "Failure!" if true;
end

def other_failing_method_test
  p "Oh no.";
  raise "Wrong!" if "spaces" > "tabs";
end
