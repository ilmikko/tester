assert_exec "./tester.sh pass" 0;
assert_exec "./tester.sh fail" 1;
assert_exec "./tester.sh multiple" 1;

assert_find "$(./tester.sh fail 2>&1)" "RuntimeError";
assert_find "$(./tester.sh multiple 2>&1)" "RuntimeError" "failing_method_test" "Failure!";
