# Check that C tests work as they are supposed to.
# In order to run a C test, we need to build the test first.
# some.test.c -> some.test.c.test
# Then, the .test file needs to be run.
assert_exec "./tester.sh pass" 0;
assert_exec "./tester.sh fail" 1;
