def passing_method_test():
    print("This method is fine.");
    assert 1 == 1

def failing_method_test():
    print("This method is not.");
    assert 0 == 1

def other_failing_method_test():
    print("Oh no.");
    assert "spaces" > "tabs"
