assert_exec "./tester.sh pass" 0;
assert_exec "./tester.sh fail" 1;
assert_exec "./tester.sh multiple" 1;

assert_find "$(./tester.sh fail 2>&1)" "AssertionError" "Traceback" "assert 1 == 0";
