function passing_method_test() {
	console.log("This method is fine.");
	return false;
}

function test_passing_method() {
	console.log("This method is fine.");
	return false;
}

function failing_method_test () {
	console.log("this method is not.");
	throw new Error("some error!");
}

function  other_failing_method_test() {
	console.log("Oh no.");
	throw new Error("some other error!");
}
