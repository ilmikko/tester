async function failing_test() {
	return new Promise(resolve => setTimeout(function() {
		throw new Error("This async function is a failure!");
	}, 10));
}
