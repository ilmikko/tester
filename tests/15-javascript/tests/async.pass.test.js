async function passing_test() {
	return new Promise(resolve => setTimeout(function() {
		console.log("Yes!");
		resolve();
	}, 10));
}

async function test_passing() {
	return new Promise(resolve => setTimeout(function() {
		console.log("Of course!");
		resolve();
	}, 10));
}
