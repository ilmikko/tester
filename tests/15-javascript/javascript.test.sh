assert_exec "./tester.sh ssync.pass" 0;
assert_exec "./tester.sh ssync.fail" 1;
assert_exec "./tester.sh multiple" 1;

assert_exec "./tester.sh async.pass" 0;
assert_exec "./tester.sh async.fail" 1;
assert_exec "./tester.sh async.immediate.fail" 1;

assert_find "$(./tester.sh ssync.fail 2>&1)" "Error: Failure!";
assert_find "$(./tester.sh multiple 2>&1)" "Error: some error!" "failing_method_test" "this method is not.";
assert_find "$(./tester.sh multiple 2>&1)" "passing_method_test" "test_passing_method";

assert_find "$(./tester.sh async.pass 2>&1)" "passing_test" "test_passing";

assert_find "$(./tester.sh async.fail 2>&1)" "Error: This async function is a failure!";
assert_find "$(./tester.sh async.immediate.fail 2>&1)" "Error: Oh no";

assert_exec "./tester.sh class.pass" 0;
assert_exec "./tester.sh class.fail" 1;
