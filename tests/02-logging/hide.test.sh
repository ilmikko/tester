# Test that irrelevant parts are hidden, for example if we do not have any unit tests,
# then the unit test section should not be shown at all.

# No tests
# assert_exec_equals "./tester.sh" "$(printf "")";
assert_equals "$(./tester.sh --output raw)" "$(printf "SUMM 0 0 0 0\nNONE")";

# Only unit tests
touch "unit.test.sh";
assert_equals "$(./tester.sh --output raw)" "$(printf "EXEC ./unit.test.sh\nPASS ./unit.test.sh\nSUMM 1 1 0 0\nPASS")";
rm "unit.test.sh";

# Only integration tests
mkdir -p "tests/00-test-test";
touch "tests/00-test-test/integration.test.sh";
assert_equals "$(./tester.sh --output raw)" "$(printf "EXEC ./tests/00-test-test/integration.test.sh\nPASS ./tests/00-test-test/integration.test.sh\nSUMM 1 1 0 0\nPASS")";

# Both unit and integration tests
touch "unit.test.sh";
assert_equals "$(./tester.sh --output raw)" "$(printf "EXEC ./unit.test.sh\nPASS ./unit.test.sh\nEXEC ./tests/00-test-test/integration.test.sh\nPASS ./tests/00-test-test/integration.test.sh\nSUMM 2 2 0 0\nPASS")";
rm "unit.test.sh";
