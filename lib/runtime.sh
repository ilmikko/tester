create_runtime_class() {
	ext="$(get_extension $1)";
	runtime_class="$run/CLASS.$ext";
	real_class="$(get_classname "$(realpath $1)" "$ext").$ext";

	if [ ! -f "$real_class" ]; then
		echo "/dev/null";
		return
	fi

	cp -r "$real_class" "$runtime_class";

	echo "$runtime_class";
}

create_runtime_test() {
	runtime_test="$run/TEST.$(get_extension $1)";
	real_test="$(realpath $1)";

	cp -r "$real_test" "$runtime_test";

	echo "$runtime_test";
}

clean_runtime() {
	rm -rf "$run";
}

init_runtime() {
	# Create a temporary runtime directory for the tests.
	run=$(mktemp --directory);

	# Copy our files over.
	for file in "$PROJECT_DIR"/*; do
		[ -e "$file" ] || continue;
		# Don't copy the tests folder.
		[ "$file" = "$TEST_FOLDER" ] && continue;
		cp -r "$file" "$run";
	done
}
