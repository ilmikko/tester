lang_c() {
	(
	echo "#include <stdio.h>";
	echo "#include <stdlib.h>";
	cat "$runtime_test";
	) > "$runtime_test.tmp";
	mv "$runtime_test.tmp" "$runtime_test";

	gcc -o "$runtime_test.test" "$runtime_test" || exit 100;
	exec="$runtime_test.test";
}
