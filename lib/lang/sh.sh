lang_sh() {
	# We want to fail in case there is any error in the test.
	(
	cat <<HERE
. $DIR/lib/assert.sh || exit 222
cd "$run"
HERE
# TODO: pull user-defined libraries here.
cat "$runtime_test" | awk '{ print $0; } /^\s*assert/ { print "EC=$?; [ $EC == 0 ] || exit $EC;" }';
cat "$runtime_class";
cat <<HERE
tests=\$(cat $runtime_test | awk '/^test[^a-zA-Z0-9][^ ]*\\s*\\(\\)\\s*{.*$/ {
	gsub(/^\\s*|\\s*\\(\\)\\s*{.*$/, "", \$0); print \$0
	next;
} /^[^ ]*test\s*\\(\\)\\s*{\\s*/ {
	gsub(/^\\s*|\\s*\\(\\)\\s*{.*$/, "", \$0); print \$0
	next;
} /^\\s*function\\s*[^ ]*test\s*(\\(\\))?\\s*{\\s*/ {
	gsub(/^\\s*function\\s*|\\s*(\\(\\))?\\s*{.*$/, "", \$0); print \$0;
	next;
} /^\\s*function\\s*test[^a-zA-Z0-9][^ ]*\\s*(\\(\\))?\\s*{.*$/ {
	gsub(/^\\s*function\\s*|\\s*(\\(\\))?\\s*{.*$/, "", \$0); print \$0;
	next;
}');

run_all() {
	while [ \$# -gt 0 ]; do
		echo "<$runtime_test> EXEC \$1";
		LOG="\$(\$1)";
		EC=\$?;
		echo "\$LOG";
		if [ "\$EC" -gt "0" ]; then
			echo "<$runtime_test> FAIL \$1";
			exit 1;
		fi
		echo "<$runtime_test> PASS \$1";
		shift;
	done
}

list_all() {
	while [ \$# -gt 0 ]; do
		echo "<$runtime_test> LIST \$1";
		shift;
	done
}

list_all \$tests;
run_all \$tests;
HERE
) > "$runtime_test.tmp";
mv "$runtime_test.tmp" "$runtime_test";

chmod +x "$runtime_test";
exec="$runtime_test";
}
