lang_rb() {
	(
	echo "#!/usr/bin/env ruby";
	cat "$runtime_test";
	cat <<HERE

pid = "<#{__FILE__}>";

tests = self.private_methods
.delete_if{ |m| s=m.to_s; s[/^test[^a-zA-Z0-9]|[^a-zA-Z0-9]test$/].nil? }
.each{ |test| puts "#{pid} LIST #{test}" };

while(!tests.empty?)
	test = tests.shift;
	begin
		puts "#{pid} EXEC #{test}";
		method(test).();
		puts "#{pid} PASS #{test}";
	rescue Exception => e
		puts "#{pid} FAIL #{test}";
		raise;
	end
end
HERE
	) > "$runtime_test.tmp";
	mv "$runtime_test.tmp" "$runtime_test";

	chmod +x "$runtime_test";
	exec="$runtime_test";
}
