lang_go() {
	(
	cat "$runtime_test";
	) > "$runtime_test.tmp";
	mv "$runtime_test.tmp" "$runtime_test";

	go build -o "$runtime_test.test" "$runtime_test" || exit 100;
	exec="$runtime_test.test";
}
