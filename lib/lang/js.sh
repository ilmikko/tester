lang_js() {
	methods=$(cat "$runtime_test" | awk "$(cat <<-AWK
		/^\s*(async\s*)?function\s*\w*\s*\(\)/
		{
			if (\$1=="async") { async=1 } else { async=0 }
			gsub(/^\s*(async\s*)?function\s*|\s*\(\).*$/, "", \$0);
			print(async, \$0)
		}
	AWK
	)" | awk '/[01]\s*test_|_test$/');

	(
	echo "#!/usr/bin/env node";
	cat <<-HERE
	// Make unhandled rejections fail the tests as well.
	process.on('unhandledRejection', error => { throw error; });
	HERE
	cat "$runtime_test";
	cat "$runtime_class";
	if [ -n "$methods" ]; then
		cat <<-HERE

		var pid = "<$runtime_test>";

		$(echo "$methods" | awk "$(cat <<-AWK
			{
				print("console.log(pid, \"LIST\", \""\$2"\");");
			}
		AWK
		)")

		$(echo "$methods" | awk "$(cat <<-AWK
			{
				print("console.log(pid, \"EXEC\", \""\$2"\");");
				if (\$1) {
					\$1 = "await";
					print("(async function(){");
				} else {
					\$1 = "";
				}

				print("try {");
				print(\$0"();");
				print("console.log(pid, \"PASS\", \""\$2"\");");
				print("}");
				print("catch(err) {");
				print("console.log(pid, \"FAIL\", \""\$2"\");");
				print("throw err;");
				print("}");

				if (\$1) {
					print("})()");
				}
		}
		AWK
		)")
		HERE
				fi
				) > "$runtime_test.tmp";
				mv "$runtime_test.tmp" "$runtime_test";

				chmod +x "$runtime_test";
				exec="$runtime_test";
			}
