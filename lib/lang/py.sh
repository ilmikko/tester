lang_py() {
	(
	echo "#!/usr/bin/env python";
	cat <<HERE
import sys;

HERE
cat "$runtime_test";
cat <<HERE

pid = "<%s>" % sys.argv[0];
tests = []
for x in dir():
	if (x.startswith("test") or x.endswith("test")) and callable(locals()[x]):
		print("%s LIST %s" % (pid, x));
		tests.append(x)

while len(tests) > 0:
	x = tests.pop()
	print("%s EXEC %s" % (pid, x));
	try:
		locals()[x]();
	except Exception as e:
		print("%s FAIL %s" % (pid, x));
		raise;
	else:
		print("%s PASS %s" % (pid, x));
		sys.stdout.flush();
HERE
) > "$runtime_test.tmp";
mv "$runtime_test.tmp" "$runtime_test";

chmod +x "$runtime_test";
exec="$runtime_test";
}
