# Initialize values.
TESTED=0;
SKIP=0;
PASS=0;
FAIL=0;

run_integration_tests=1;
run_unit_tests=1;

. "$DIR/lib/lang/c.sh" || exit 222;
. "$DIR/lib/lang/go.sh" || exit 222;
. "$DIR/lib/lang/js.sh" || exit 222;
. "$DIR/lib/lang/py.sh" || exit 222;
. "$DIR/lib/lang/rb.sh" || exit 222;
. "$DIR/lib/lang/sh.sh" || exit 222;
. "$DIR/lib/lang/test.sh" || exit 222;

get_extension() {
	echo "$1" | awk -F. '{ print $NF }';
}

get_classname() {
	echo "$1" | sed -e 's/[_.]test.'"$2"'//g';
}

test_fail() {
	FAIL=$((FAIL+1));
	output_test_fail "$1";
}

test_pass() {
	PASS=$((PASS+1));
	output_test_pass "$1";
}

test_parse() {
	awk '{ print $0; if ($1 == "<'"$1"'>" && $2 == "EC") { exit $3 }; fflush(stdout); system(""); }';
}

run_test() {
	output_test_run "$1";

	should_skip "$1" && return 0;

	runtime_test=$(create_runtime_test $1);
	runtime_class=$(create_runtime_class $1);
	extension=$(get_extension $1);

	case $extension in
		c|go|js|py|rb|sh|test)
			lang_$extension;
			;;
		*)
			fail "[1;31mUnknown extension $extension! $1[m";
			;;
	esac

	TESTED=$((TESTED+1));
	set -o pipefail;
	( "$exec" 2>&1; echo "<$runtime_test>" "EC" "$?" ) | test_parse "$runtime_test" | output_test_output "$runtime_test" "$1";
	EC=$?;
	set +o pipefail;

	if [ $EC = 0 ]; then
		test_pass "$1";
		return 0;
	else
		test_fail "$1";
		return 1;
	fi
}

run_all_tests() {
	output_before_test;

	init_runtime;

	[ "$run_unit_tests" = 0 ] || run_unit_tests;
	[ "$run_integration_tests" = 0 ] || run_integration_tests;

	clean_runtime;

	output_after_test;
}
