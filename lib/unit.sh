# Unit tests.

get_unit_tests_in_dir() {
	find "$1" -type f -iname "*[._]test*" -not -iname "*\.pyc" -not -iwholename "*/.git/*" -not -iwholename "*/$(basename "$TEST_FOLDER")/*/*";
}

# List all unit tests.
get_unit_tests() {
	tests=$(get_unit_tests_in_dir "$PROJECT_DIR");
	for test in $tests; do
		[ -f "$test" ] || continue;
		echo $test;
	done
}

run_unit_tests() {
	unit_tests="$(get_unit_tests)";

	[ -n "$unit_tests" ] || return 0;
	bold "Running unit tests:";

	for original_test in $unit_tests; do
		run_test $original_test;
	done

	bold;
}
