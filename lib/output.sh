output_after_test() {
	output_summary;
	output_faillog;
	output_results;
}

output_before_test() {
	[ -z "$filters" ] || echo "Testing for '$filters'...";
}

output_set_type() {
	case "$1" in
		detailed|raw|short)
			OUTPUT_TYPE="$1";
			. "$DIR/lib/output/$1.sh" || exit 222;
			;;
		*)
			fail "Unknown output type: $1";
			;;
	esac
}
