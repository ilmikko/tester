should_skip() {
	# Anything with a 'SKIP' in the path will get skipped.
	if echo "$1" | grep -q "SKIP"; then
		output_test_skip "$1";
		SKIP=$((SKIP+1));
		return 0;
	fi

	# Anything that does not pass the filters will get skipped.
	if [ -z "$(echo "$1" | awk "/$filters/")" ]; then
		output_test_skip "$1";
		SKIP=$((SKIP+1));
		return 0;
	fi

	# Do not skip.
	return 1;
}
