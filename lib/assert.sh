# Assertions.

escape() {
	sed 's//^[/g';
}

print_bold() {
	echo "[1;31m$@[m";
}

assert_success() {
	exec="$@";
	LOG="$($exec 2>&1)";
	EC=$?;
	if [ $EC -gt 0 ]; then
		print_bold "Assert success failed:";
		print_bold "$@ failed with code $EC";
		echo "Log:";
		echo "$(echo "$LOG" | escape)";
		return 1;
	fi
	return 0;
}

assert_fail() {
	exec="$@";
	LOG="$($exec 2>&1)";
	EC=$?;
	if [ ! $EC -gt 0 ]; then
		print_bold "Assert fail failed:";
		print_bold "$@ succeeded with code $EC";
		echo "Log:";
		echo "$(echo "$LOG" | escape)";
		return 1;
	fi
	return 0;
}

assert_equals() {
	got=$1;
	want=$2;
	[ "$got" == "$want" ];
	EC=$?;
	if [ $EC -gt 0 ]; then
		print_bold "Assert equals failed:";
		echo "Want: '$(echo "$want" | escape)'";
		echo "Got : '$(echo "$got" | escape)'";
	fi
	return $EC;
}

assert_find() {
	haystack=$1;
	shift;
	for needle in "$@"; do
		echo "$haystack" | grep -q "$needle";
		EC=$?;
		if [ $EC -gt 0 ]; then
			print_bold "Assert find failed:";
			echo "Haystack: '$(echo "$haystack" | escape)'";
			echo "Needle  : '$(echo "$needle" | escape)'";
			return $EC;
		fi
	done
	return $EC;
}

assert_exec() {
	exec=$1;
	want=$2;
	[ -z "$want" ] && want=0;
	LOG="$($exec 2>&1)";
	got=$?;
	[ "$got" == "$want" ];
	EC=$?;
	if [ $EC -gt 0 ]; then
		print_bold "Assert exec failed:";
		echo "Return code: '$got'";
		echo "Want       : '$want'";
		echo "Log        :";
		echo "$(echo "$LOG" | escape)";
	fi
	return $EC;
}

assert_exec_find() {
	exec=$1;
	shift;
	assert_find "$($exec 2>&1)" "$@";
}

assert_exec_equals() {
	exec=$1;
	shift;
	assert_equals "$($exec 2>&1)" "$@";
}

assert_file() {
	for file in $@; do
		if [ ! -f "$file" ]; then
			print_bold "File assertion failed:";
			echo "$(realpath $file) does not exist.";
			return 1;
		fi
	done
}

assert_folder() {
	for folder in $@; do
		if [ ! -d "$folder" ]; then
			print_bold "Folder assertion failed:";
			echo "$(realpath $folder) does not exist.";
			return 1;
		fi
	done
}

assert_not() {
	! "$@";
	EC=$?;
	if [ $EC -gt 0 ]; then
		print_bold "Assert not failed:";
		echo "Assertion: $(echo "$1" | escape)";
		echo "\$1       : $(echo "$2" | escape)";
		echo "\$2       : $(echo "$3" | escape)";
		echo "\$3       : $(echo "$4" | escape)";
		echo "Code     : $EC";
		return 1;
	fi
}
