# List all integration tests.
get_integration_tests() {
	for folder in "$TEST_FOLDER/"*; do
		[ -d "$folder" ] || continue;
		tests="$(get_integration_tests_in_dir "$folder")";
		[ -n "$tests" ] || continue;
		echo "$folder";
	done
}

get_integration_tests_in_dir() {
	find "$1" -maxdepth 1 -type f -iname "*[._]test*" -not -iname "*\.pyc" -not -iwholename "*/.git/*" -not -iwholename "$1/$(basename "$TEST_FOLDER")/*";
}

run_integration_tests() {
	# Sanity check so we wont remove /.
	[ -n "$run" ] || fail "Critical error; \$run was empty.";

	if [ ! -d "$TEST_FOLDER" ]; then
		return 0;
	fi

	integration_tests="$(get_integration_tests)";

	[ -n "$integration_tests" ] || return 0;
	bold "Running integration tests:";

	for test_folder in $integration_tests; do
		# Copy over the contents of the test directory.
		# We also check that the files are not in conflict.
		# Any conflicting files that would be replaced by the copy
		# will cause the test to fail.
		for path in $test_folder/*; do
			[ -e "$path" ] || continue;

			file="$(basename "$path")";
			if [ -e "$run/$file" ]; then
				error "$run/$file is in conflict with $path.";
				return;
			fi

			cp -r "$path" "$run/$file";
		done

		tests="$(get_integration_tests_in_dir "$test_folder")";
		for test in $tests; do
			path="$run/$(basename "$test")";
			[ -f "$path" ] || continue;
			rm "$path";
		done

		# Then, run all the tests in the directory.
		tests="$(get_integration_tests_in_dir "$test_folder")";
		for test in $tests; do
			run_test "$test";
		done

		# Finally, clean up.
		for path in $test_folder/*; do
			[ -e "$path" ] || continue;

			file="$(basename "$path")";
			rm -rf "$run/$file";
		done
	done

	bold;
}
