FAILLOG=$(mktemp /tmp/faillog.XXXXXX);
trap "rm $FAILLOG" EXIT;

bold() {
	log "[1m"$@"[m";
}

error() {
	log "[31m"$@"[m";
}

fail() {
	error "$@";
	exit 1;
}

log() {
	echo "$@" 1>&2;
}

summary() {
	echo "Test summary for $TESTED tests:";
	if [ $PASS -gt 0 ]; then
		echo "[1;32m  Passes: $PASS[m";
	else
		echo "  Passes: 0";
	fi
	if [ $FAIL -gt 0 ]; then
		echo "[1;31mFailures: $FAIL[m";
	else
		echo "Failures: 0";
	fi
	if [ $SKIP -gt 0 ]; then
		echo "[1;33m   Skips: $SKIP[m";
	fi
}

print_end() {
	if [ $FAIL -gt 0 ]; then
		if [ -z "$filters" ]; then
			echo "[1;31mFailure log:[m";
			cat "$FAILLOG";
			echo;
		fi
		echo "[1;31mFailures during testing.[m";
		exit $FAIL;
	else
		if [ $TESTED -gt 0 ]; then
			echo "[32mAll tests pass! :D[m";
		else
			echo "[1;33mNo tests were run.[m";
		fi
	fi
}

# TODO: Get rid of this monstrosity
print_log() {
	if [ -z "$filters" ]; then
		awk '{
			if (index($0, "<'"$exec"'>")) {
				switch($2) {
				case "PASS":
					printf("[32m,[m");
					break;
				case "FAIL":
					printf(" [1m\\- %s[m\n", $3) > "/dev/stderr";
					printf("[31m![m");
					break;
				case "SKIP":
					printf("[33m-[m");
					break;
				}
			} else {
				printf(" .   %s\n", $0) > "/dev/stderr"
			}
		}';
	else
		awk '{
			if (index($0, "<'"$exec"'>")) {
				if (!subtests) {
					printf("\n");
					printf(" |\n");
				}
				subtests=1;
				printf(" |- ");
				switch($2) {
				case "PASS":
					printf("[32mPASS[m");
					break;
				case "FAIL":
					printf("[1m%s[m\n", $3) > "/dev/stderr"
					printf("[1;31mFAIL[0;31m");
					break;
				case "SKIP":
					printf("[33mSKIP[0;2m");
				}
				printf(" %s[m", $3);
				printf("\n");
			} else {
				print(".", $0) > "/dev/stderr"
			}
		} END {
			if (subtests) {
				printf(" |\n");
				printf(" \\= ");
			}
		}';
	fi
}

print_faillog() {
	if [ -z "$filters" ]; then
		printf "\n[1m%s[m\n" "$1" >> "$FAILLOG";
		cat "$LOG" >> "$FAILLOG";
		printf "%s" "$(echo "$1" | sed 's/./-/g')" >> "$FAILLOG";
	else
		cat "$LOG";
	fi
	[ -f "$LOG" ] && rm "$LOG";
}

print_filters() {
	[ -n "$filters" ] && bold "Testing for '$filters'...";
}

print_test() {
	[ -z "$filters" ] || printf "$1 ";
}

print_pass() {
	[ -z "$filters" ] && printf "[32m.[m" || echo "[1;32mPASS[m";
}

print_skip() {
	[ -z "$filters" ] && printf "[1;33m?[m";
}

print_fail() {
	[ -z "$filters" ] && printf "[1;31m![m" || echo "[1;31mFAIL[m";
	print_faillog $1;
}
