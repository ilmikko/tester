# A more detailed format, with inline failures.
output_test_run() {
	printf "$1 ";
}

output_test_fail() {
	echo "[1;31mFAIL[m";
}

output_test_pass() {
	echo "[32mPASS[m";
}

output_test_skip() {
	printf "\r[K";
}

output_test_output() {
	awk 'function subtest() {
			if (!subtests) {
				subtests=1;
				printf("::");
			}
		}
		{
		if ($1 == "<'"$1"'>") {
			switch($2) {
				case "EC":
					if (subtests) printf("\n -> ", $3);
					if ($3 > 0) {
						print "---'"$2"'---" >> "'$FAILLOG'";
						if (failing_test) print "("failing_test")" >> "'$FAILLOG'";
						for (i = 0; i < out; i++) {
							print i,lines[i] >> "'$FAILLOG'";
						}
						print "---'"$(echo $2 | sed -e 's/./-/g')"'---" >> "'$FAILLOG'";
					}
					exit;
				case "EXEC":
					subtest();
					printf("\n%s ", $3);
					break;
				case "FAIL":
					failing_test=$3;
					subtest();
					printf("%s", "[31mFAIL[m");
					break;
				case "PASS":
					out=0;
					subtest();
					printf("%s", "[32mPASS[m");
					break;
				case "SKIP":
					subtest();
					printf("%s", "[33mSKIP[m");
					break;
				default:
					lines[out++]=$0;
					break;
			}
			next;
		}
		lines[out++]=$0;
	}';
}

output_faillog() {
	faillog="$(cat $FAILLOG)";
	[ -n "$faillog" ] || return;
	log;
	log "Failure log:";
	log "$faillog";
}

output_results() {
	log;
	if [ "$FAIL" -gt 0 ]; then
		log "[1;31mFailures during testing.[m";
	elif [ "$TESTED" = "0" ]; then
		log "[1;33mNo tests were run.[m";
	else
		log "[1;32mAll tests pass! :D[m";
	fi
}

output_summary() {
	log "Test summary for $TESTED test(s):";

	[ $PASS -gt 0 ] && col="[1;32m" || col="[m";
	log "$col""  Passes: $PASS""[m";

	[ $FAIL -gt 0 ] && col="[1;31m" || col="[m";
	log "$col""Failures: $FAIL""[m";

	[ $SKIP -gt 0 ] && col="[1;33m" || col="[m";
	log "$col""   Skips: $SKIP""[m";
}
