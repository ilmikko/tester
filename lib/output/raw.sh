# Raw format for machine reading.
output_test_run() {
	echo "EXEC $1";
}

output_test_fail() {
	echo "FAIL $1";
}

output_test_pass() {
	echo "PASS $1";
}

output_test_skip() {
	echo "SKIP $1";
}

output_test_output() {
	awk '{ if ($1 != "<'$1'>" || $2 != "EC") { print $0 } }';
}

output_faillog() {
	:
}

output_results() {
	if [ "$FAIL" -gt 0 ]; then
		echo "FAIL";
	elif [ "$TESTED" = "0" ]; then
		echo "NONE";
	else
		echo "PASS";
	fi
}

output_summary() {
	echo "SUMM $TESTED $PASS $FAIL $SKIP";
}
