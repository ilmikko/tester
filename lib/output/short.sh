# More concise format.
output_test_run() {
	printf "";
}

output_test_fail() {
	printf "[1;31m![m";
}

output_test_pass() {
	printf "[32m:[m";
}

output_test_skip() {
	printf "[1;33m?[m";
}

output_test_output() {
	awk '{
		if ($1 == "<'"$1"'>") {
			switch($2) {
				case "EC":
					if ($3 > 0) {
						print "'"$2"'" >> "'$FAILLOG'";
						if (failing_test) print failing_test >> "'$FAILLOG'";
						for (i = 0; i < out; i++) {
							print i,lines[i] >> "'$FAILLOG'";
						}
					}
					exit;
				case "PASS":
					printf("[32m.[m");
					break;
				case "FAIL":
					failing_test=$3;
					printf("[31m,[m");
					break;
				case "SKIP":
					printf("[33m-[m");
					break;
				default:
					lines[out++]=$0;
					break;
			}
			next;
		}
		lines[out++]=$0;
	}';
}

output_faillog() {
	faillog="$(cat $FAILLOG)";
	[ -n "$faillog" ] || return;
	log;
	log "Failure log:";
	log "$faillog";
}

output_results() {
	log;
	if [ "$FAIL" -gt 0 ]; then
		log "[1;31mFailures during testing.[m";
	elif [ "$TESTED" = "0" ]; then
		log "[1;33mNo tests were run.[m";
	else
		log "[1;32mAll tests pass! :D[m";
	fi
}

output_summary() {
	log;
	log "Test summary for $TESTED test(s):";

	[ $PASS -gt 0 ] && col="[1;32m" || col="[m";
	log "$col""  Passes: $PASS""[m";

	[ $FAIL -gt 0 ] && col="[1;31m" || col="[m";
	log "$col""Failures: $FAIL""[m";

	[ $SKIP -gt 0 ] && col="[1;33m" || return;
	log "$col""   Skips: $SKIP""[m";
}
